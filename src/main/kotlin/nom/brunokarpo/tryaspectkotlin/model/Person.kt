package nom.brunokarpo.tryaspectkotlin.model

import java.util.*

data class Person(
        var name: String? = null,
        var age: Integer? = null,
        var uuid: UUID? = null
)