package nom.brunokarpo.tryaspectkotlin.controller.events.listeners

import nom.brunokarpo.tryaspectkotlin.controller.events.AuditEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component

@Component
class AuditEventListener: ApplicationListener<AuditEvent<*>> {

    private val log: Logger = LoggerFactory.getLogger(AuditEventListener::class.java)

    override fun onApplicationEvent(event: AuditEvent<*>) {
        log.info("\n*******************************\n" +
                "An event recived" +
                "\n*******************************\n")

        log.info("Event: user-id: ${event.userId}\n" +
                "system-id: ${event.systemId}\n" +
                "dto: ${event.dto}\n" +
                "request: ${event.requestMethod}\n" +
                "endpoint: ${event.endpoint}\n" +
                "System: ${event.system}")
    }
}