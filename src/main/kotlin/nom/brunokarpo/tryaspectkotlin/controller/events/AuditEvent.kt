package nom.brunokarpo.tryaspectkotlin.controller.events

import org.springframework.context.ApplicationEvent
import org.springframework.stereotype.Component

class AuditEvent<out T>(source: Any,
                        val systemId: String? = null,
                        val userId: String?? = null,
                        val dto: T,
                        val endpoint: String,
                        val requestMethod: String,
                        val system: String): ApplicationEvent(source)