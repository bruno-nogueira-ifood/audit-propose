package nom.brunokarpo.tryaspectkotlin.controller

import nom.brunokarpo.tryaspectkotlin.controller.events.AuditEvent
import nom.brunokarpo.tryaspectkotlin.model.Person
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/person")
class PersonRequest {

    private val LOG: Logger = LoggerFactory.getLogger(PersonRequest::class.java)

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @PostMapping("/{name}/{age}")
    fun savePerson(@PathVariable("name") name: String,
                   @PathVariable("age") age: Integer,
                   @RequestHeader("system-id", required = false) systemId: String?,
                   @RequestHeader("user-id", required = false) userId: String?,
                   request: HttpServletRequest
    ): ResponseEntity<Person> {

        LOG.info("\n======================\n" +
                "Request name: ${name}\n" +
                "Request age: ${age}\n" +
                "Header systemId: ${systemId}\n" +
                "Request userId: ${userId}\n" +
                "=======================")

        var uuid = UUID.randomUUID()
        val person = Person(name, age, uuid)

        publisher.publishEvent(AuditEvent(this, systemId, userId, person, request.requestURI, request.method,"Person system"))

        return ResponseEntity.created(URI.create( "/person/${uuid}")).body(person)
    }

}