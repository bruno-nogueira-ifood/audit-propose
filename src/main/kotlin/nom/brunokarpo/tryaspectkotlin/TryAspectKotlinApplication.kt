package nom.brunokarpo.tryaspectkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TryAspectKotlinApplication

fun main(args: Array<String>) {
    runApplication<TryAspectKotlinApplication>(*args)
}
