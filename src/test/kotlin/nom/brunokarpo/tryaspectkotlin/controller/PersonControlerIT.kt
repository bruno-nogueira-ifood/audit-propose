package nom.brunokarpo.tryaspectkotlin.controller

import io.restassured.RestAssured
import nom.brunokarpo.tryaspectkotlin.TryAspectKotlinApplicationTests
import org.hamcrest.Matchers
import org.junit.Test
import org.springframework.http.HttpStatus
import java.util.*

class PersonControlerIT: TryAspectKotlinApplicationTests() {

    @Test
    fun shouldSaveNewPerson() {
        RestAssured.given()
                .header("system-id", "my-system-id")
                .header("user-id", "my-user-id")
                .pathParam("name", "Bruno Nogueira")
                .pathParam("age", 28)
                .`when`()
                .post("/person/{name}/{age}")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .and()
                .log()
                .headers()
                .and()
                .header("location", Matchers.notNullValue(String.javaClass))
                .body("name", Matchers.equalTo("Bruno Nogueira"),
                        "age", Matchers.equalTo(28),
                        "uuid", Matchers.notNullValue(UUID::class.java))
    }
}